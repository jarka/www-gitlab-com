---
layout: job_page
title: "Frontend Engineer"
---

## Responsibilities

* Fix prioritized issues from the issue tracker.
* Write the code to complete scheduled direction issues chosen by our scheduling committee, assigned to you by the frontend lead.
* Continually improve the quality of GitLab by using discretion of where you think changes are needed.
* Create high quality frontend code.
* Frontend Design Specialist: Implement the interfaces in GitLab proposed by UX Engineers and contributors.
* Frontend Design Specialist: Improve the [static website of GitLab](https://about.gitlab.com/) based on the collaborations with the the Designer and CMO.

## Workflow

- You work on issues tagged with 'Frontend' on [CE](https://gitlab.com/gitlab-org/gitlab-ce/issues?label_name=Frontend) and [EE](https://gitlab.com/gitlab-org/gitlab-ee/issues?label_name=Frontend).
- The priority of the issues tagged with this label can be found in [the handbook under GitLab Workflow](https://about.gitlab.com/handbook/#prioritize).
- When done with a frontend issue remove the 'Frontend' label and add the next [workflow label](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/PROCESS.md#workflow-labels) which is probably the 'Developer' label.

## Requirements for Applicants

* Know how to use CSS effectively
* Expert knowledge of JavaScript
* Basic knowledge of Vue.js is a plus but not a requirement
* Collaborate effectively with UX Designers, Developers, and Designers
* Be able to work with the rest of the community
* Knowledge of Ruby on Rails is a plus
* You share our [values](/handbook/#values), and work in accordance with those values.

## Relevant links

- [Engineering Handbook](/handbook/engineering)
- [Engineering Workflow](/handbook/engineering/workflow)

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

* Qualified applicants receive a short questionnaire from our Global Recruiters
* Selected candidates will be invited to schedule a 45 minute [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a 45 minute first interview with a Frontend Engineer
* Candidates will then be invited to schedule a 1 hour technical interview with the Frontend Lead
* Candidates will be invited to schedule a third 45 minute interview with our VP of Engineering
* Finally, candidates will schedule a 50 minute interview with our CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).
