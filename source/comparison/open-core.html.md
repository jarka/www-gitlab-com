---
layout: comparison_page
title: GitLab compared to other tools
suppress_header: true
image_title: '/images/comparison/title_image.png'
extra_css:
  - compared.css
---

## Open-Core
GitLab is an open-core product whereas GitHub and BitBucket are closed-source products. The GitLab Community Edition is fully open-source, and the Enterprise Edition is closed-source.

### Access to the Source Code
You can see the GitLab Community Edition and Enterprise source code
at any time, even on your own server.

### Fully Modifiable
Unlike closed-source software, you can modify
GitLab's source code. Be it right on the server or by forking
our repositories. You can add features and make customizations.
We do recommend that you try to merge your changes back into the
main source code, so that others can benefit and your instance
stays easy to maintain and update.

### Viable long term
GitLab has a solid community with hundreds of thousands of organisations
using and often contributing to the software. This means that GitLab is much
more viable for long term usage, as it's not reliable on single company supporting it.

### New Stable version every month
GitLab releases a new stable version every single month, full of improvements,
new features and fixes. This allows GitLab to move fast and respond to customer
requests extremely quickly.

### Build with a community
GitLab is built by hundreds of people every month. Customers, users and GitLab Inc all contribute to every release. This leads to features that organisations actually need, such as easy, yet powerful user management.
